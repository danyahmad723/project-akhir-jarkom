﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class UdpSrvr
{
    public static void Main()
    {
        int recv;
        string us, user;
        string masuk;
        byte[] username = new byte[1024];
        byte[] data = new byte[1024];
        IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 9050);

        Socket newsock = new Socket(AddressFamily.InterNetwork,
                        SocketType.Dgram, ProtocolType.Udp);

        newsock.Bind(ipep);
        Console.WriteLine("Waiting for a client...");

        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        EndPoint Remote = (EndPoint)(sender);

        recv = newsock.ReceiveFrom(data, ref Remote);

        username = new byte[1024];
        int terima = newsock.ReceiveFrom(username, ref Remote);
        us = Encoding.ASCII.GetString(username, 0, terima);
        Console.WriteLine("username Client :" + Encoding.ASCII.GetString(username, 0, terima));

        Console.WriteLine(" input Username : ");
        user = Console.ReadLine();
        newsock.SendTo(Encoding.ASCII.GetBytes(user), Remote);


        string welcome = "Connected!";
        data = Encoding.ASCII.GetBytes(welcome);
        newsock.SendTo(data, data.Length, SocketFlags.None, Remote);
        while (true)
        {
            data = new byte[1024];
            recv = newsock.ReceiveFrom(data, ref Remote);
            Console.WriteLine(user + ":" + Encoding.ASCII.GetString(data, 0, recv));
            Console.WriteLine(user + " : ");
            masuk = Console.ReadLine();
            newsock.SendTo(Encoding.ASCII.GetBytes(masuk), Remote);
        }
    }
}